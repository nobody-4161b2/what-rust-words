# what-rust-words

the [what3words](https://what3words.com/) algorithm in rust

```rust
use what_rust_words::What3Words;

fn main() {
  let w3w = What3Words::default();
  println!("{}.{}.{}", w3w.coord_to_words(37.234332, -115.806657)); // joyful.nail.harmonica
  println!("{}, {}", w3w.words_to_coord(["joyful", "nail", "harmonica"])) // 37.234332, -115.806657
}
```

## what3words the company

what3words push emergency services to use their tech  
such societally important technology should belong to the commons

what3words make a simple mathematical transform & table look up  an online query  
this is a massive waste of energy  
please consider alternative tech or an offline implementation such as this

---

thoughts are free. they should remain free, and be given freely.

this repository exists in many places to ensure integrity of the release  
host your own mirrors too

- https://codeberg.org/nobody-4161b2/what-rust-words.git
- https://git.vbrlabs.io/nobody-4161b2/what-rust-words.git
- https://git.acinex.com/nobody-4161b2/what-rust-words.git
- https://git.antkeeper.com/nobody-4161b2/what-rust-words.git
- https://git.lost.host/nobody-4161b2/what-rust-words.git
- https://git.minie.re/nobody-4161b2/what-rust-words.git
- https://git.byzero.dev/nobody-4161b2/what-rust-words.git
- http://git.react-logic.com/nobody-4161b2/what-rust-words.git
