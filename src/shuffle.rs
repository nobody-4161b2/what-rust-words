use std::io::{Read, Result};

use byteorder::{LittleEndian as LE, ReadBytesExt};

pub struct ShuffleBlock {
    start: u64,
    size: u64,
    f: u64,
    r: u64,
}

pub fn build_shuffle_blocks(
    num_blocks: usize,
    mut raw_data: impl Read,
) -> Result<Vec<ShuffleBlock>> {
    let mut blocks = Vec::with_capacity(num_blocks);

    for _ in 0..num_blocks {
        let start = raw_data.read_u64::<LE>()?;
        let size = raw_data.read_u64::<LE>()?;
        let f = raw_data.read_u64::<LE>()?;
        let r = raw_data.read_u64::<LE>()?;
        blocks.push(ShuffleBlock { start, size, f, r });
    }

    Ok(blocks)
}

pub fn shuffle(blocks: &[ShuffleBlock], n: u64) -> u64 {
    for block in blocks.iter() {
        if n >= block.start && n <= block.start + block.size {
            let mult_result: u128 = (n - block.start) as u128 * block.f as u128;
            let wrapped = mult_result % block.size as u128;
            return wrapped as u64 + block.start;
        }
    }

    panic!("Unable to shuffle n where n = {}", n);
}

pub fn unshuffle(blocks: &[ShuffleBlock], m: u64) -> u64 {
    for block in blocks.iter() {
        if m >= block.start && m < block.start + block.size {
            let big = (m - block.start) as u128 * block.r as u128;
            return (big % block.size as u128) as u64 + block.start;
        }
    }

    panic!("Unable to shuffle m where m = {}", m);
}
