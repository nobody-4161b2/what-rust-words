use std::collections::BTreeMap;

pub struct WordData {
    by_idx: Vec<&'static str>,
    to_idx: BTreeMap<&'static str, usize>,
}

pub fn build_word_data(raw_word_data: &'static str) -> WordData {
    let words: Vec<_> = raw_word_data.lines().collect();
    let idx_lut = BTreeMap::from_iter(words.iter().enumerate().map(|(i, w)| (*w, i)));
    WordData {
        by_idx: words,
        to_idx: idx_lut,
    }
}

pub fn ijk_to_words(data: &WordData, i: u64, j: u64, k: u64) -> [&'static str; 3] {
    [
        data.by_idx[i as usize],
        data.by_idx[j as usize],
        data.by_idx[k as usize],
    ]
}

pub fn words_to_ijk(data: &WordData, words: [&str; 3]) -> [u64; 3] {
    words.map(|w| {
        *data
            .to_idx
            .get(w)
            .unwrap_or_else(|| panic!("Word '{}' does not exist?", w)) as u64
    })
}
