// https://patents.google.com/patent/US9883333B2
#![allow(non_snake_case, clippy::many_single_char_names)] // math

pub struct What3Words {
    regions: RegionData,
    words: WordData,
    shuffle_blocks: Vec<ShuffleBlock>,
}

impl What3Words {
    pub fn new(regions: RegionData, words: WordData, shuffle_blocks: Vec<ShuffleBlock>) -> Self {
        Self {
            regions,
            words,
            shuffle_blocks,
        }
    }
}

mod region;
mod shuffle;
mod word_encode;
mod words;

pub use region::{build_region_data, RegionData};
pub use shuffle::{build_shuffle_blocks, ShuffleBlock};
pub use words::{build_word_data, WordData};

impl What3Words {
    pub fn coord_to_words(&self, lat: f64, lon: f64) -> [&'static str; 3] {
        let [X, Y, x, y] = region::coord_to_cell_info(lat, lon);
        let n = region::cell_info_to_n(&self.regions, X, Y, x, y);
        let m = shuffle::shuffle(&self.shuffle_blocks, n);
        let [i, j, k] = word_encode::m_to_ijk(m);
        words::ijk_to_words(&self.words, i, j, k)
    }

    pub fn words_to_coord(&self, words: [&str; 3]) -> (f64, f64) {
        let [i, j, k] = words::words_to_ijk(&self.words, words);
        let m = word_encode::ijk_to_m(i, j, k);
        let n = shuffle::unshuffle(&self.shuffle_blocks, m);
        let [X, Y, x, y] = region::n_to_cell_info(&self.regions, n);
        region::cell_info_to_coord(X, Y, x, y)
    }
}

#[cfg(feature = "proprietary")]
mod proprietary;
