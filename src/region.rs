use byteorder::{LittleEndian as LE, ReadBytesExt};
use std::{
    io::{Read, Result},
    rc::Rc,
};

const LON_SLICES: usize = 1546;
const LAT_SLICES: usize = 4320;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct Region {
    x: u16,
    y: u16,
    length: u16,
    q: u64,
}

pub struct RegionData {
    all: Vec<Rc<Region>>,
    by_y: [Vec<Rc<Region>>; LAT_SLICES],
}

fn W(Y: f64) -> u64 {
    f64::max(
        1.0,
        f64::floor(LON_SLICES as f64 * f64::sin(f64::to_radians((Y + 0.5) / 24.0))),
    ) as u64
}

pub fn build_region_data(num_regions: usize, mut raw_data: impl Read) -> Result<RegionData> {
    let mut sequential = Vec::new();
    let mut by_y = array_init::array_init(|_| Vec::new());

    let mut q = 0;

    for _ in 0..num_regions {
        let x = raw_data.read_u16::<LE>()?;
        let y = raw_data.read_u16::<LE>()?;
        let length = raw_data.read_u16::<LE>()?;

        let region = Rc::new(Region { x, y, length, q });
        sequential.push(region.clone());
        by_y[y as usize].push(region);

        q += length as u64 * W(y as f64) * LON_SLICES as u64;
    }

    by_y.iter_mut().for_each(|v| v.sort());

    Ok(RegionData {
        all: sequential,
        by_y,
    })
}

pub fn coord_to_cell_info(lat: f64, lon: f64) -> [u64; 4] {
    // "The cell identity value is a pair of integers X and Y,
    // and the cell position value is a pair of integers x and y."

    let lon24 = (lon + 180.0) * 24.0;
    let lat24 = (lat + 90.0) * 24.0;

    let X = lon24.floor() as u64;
    let Y = lat24.floor() as u64;

    let x = f64::floor(W(Y as f64) as f64 * lon24.fract()) as u64;
    let y = f64::floor(LON_SLICES as f64 * lat24.fract()) as u64;

    [X, Y, x, y]
}

pub fn cell_info_to_n(data: &RegionData, X: u64, Y: u64, x: u64, y: u64) -> u64 {
    let q = cell_identity_to_q(data, X, Y);
    q + x * LON_SLICES as u64 + y
}

fn cell_identity_to_q(data: &RegionData, X: u64, Y: u64) -> u64 {
    let mut min_i = 0;
    let mut max_i = data.by_y[Y as usize].len();

    while min_i < max_i {
        let i = (min_i + max_i) / 2;

        let Region { x, y, length, q } = *data.by_y[Y as usize][i];
        let (x, y, length) = (x as u64, y as u64, length as u64);

        if x <= X && x + length > X && y == Y {
            return q + (X - x) * W(Y as f64) * LON_SLICES as u64;
        } else if X < x {
            max_i = i
        } else {
            min_i = i + 1
        }
    }

    panic!("No cell ({}, {})", X, Y);
}

pub fn cell_info_to_coord(X: u64, Y: u64, x: u64, y: u64) -> (f64, f64) {
    let lat = (Y as f64 + (y as f64 + 0.5) / LON_SLICES as f64) / 24.0 - 90.0;
    let lon = (X as f64 + (x as f64 + 0.5) / W(Y as f64) as f64) / 24.0 - 180.0;
    (lat, lon)
}

pub fn n_to_cell_info(data: &RegionData, n: u64) -> [u64; 4] {
    let ([X, Y], q) = n_to_cell_identity_and_q(data, n);
    let n = n - q;
    let x = n / LON_SLICES as u64;
    let y = n - x * LON_SLICES as u64;
    [X, Y, x, y]
}

fn n_to_cell_identity_and_q(data: &RegionData, n: u64) -> ([u64; 2], u64) {
    let mut min_i = 0usize;
    let mut max_i = data.all.len();

    while min_i < max_i {
        let i = (min_i + max_i) / 2;

        let Region {
            x,
            y,
            length,
            q: start_q,
        } = *data.all[i];

        let one_cell_size = W(y.into()) * LON_SLICES as u64;
        let end_q = start_q + length as u64 * one_cell_size;

        if n >= start_q && n < end_q {
            let i = (n - start_q) / one_cell_size;
            return (
                [x as u64 + i as u64, y as u64],
                start_q + i as u64 * one_cell_size,
            );
        } else if n < start_q {
            max_i = i;
        } else {
            min_i = i + 1;
        }
    }

    panic!("No such cell where n = {}", n);
}
