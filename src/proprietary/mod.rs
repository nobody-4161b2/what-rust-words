use crate::{build_region_data, build_shuffle_blocks, build_word_data, What3Words};

impl Default for What3Words {
    fn default() -> Self {
        use std::io::Cursor;

        const REGION_DATA: &[u8] = include_bytes!("./region_data.bin");
        const WORDS_EN: &str = include_str!("./words_en.txt");
        const SHUFFLE_BLOCKS: &[u8] = include_bytes!("./shuffle_blocks.bin");

        let regions = build_region_data(REGION_DATA.len() / 6, Cursor::new(REGION_DATA))
            .expect("Failed to build region data");
        let words = build_word_data(WORDS_EN);
        let shuffle_blocks =
            build_shuffle_blocks(SHUFFLE_BLOCKS.len() / 32, Cursor::new(SHUFFLE_BLOCKS))
                .expect("Failed to build shuffle blocks");

        Self::new(regions, words, shuffle_blocks)
    }
}
