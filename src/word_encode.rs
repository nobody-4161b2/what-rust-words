pub fn ijk_to_m(i: u64, j: u64, k: u64) -> u64 {
    let l = u64::max(i, u64::max(j, k));
    let l_sq = l * l;
    let l_cb = l_sq * l;

    if i == l {
        l_cb + (l + 1) * j + k
    } else if j == l {
        l_cb + l_sq + 2 * l + 1 + (l + 1) * i + k
    } else {
        l_cb + 2 * l_sq + 3 * l + 1 + l * i + j
    }
}

pub fn m_to_ijk(m: u64) -> [u64; 3] {
    let l = (m as f64).cbrt().floor() as u64;
    let l_sq = l * l;
    let l_cb = l_sq * l;

    if l_cb <= m && m < l_cb + l_sq + 2 * l + 1 {
        let r = m - l_cb;
        [l, r / (l + 1), r % (l + 1)]
    } else if l_cb + l_sq + 2 * l < m && m < l_cb + 2 * l_sq + 3 * l + 1 {
        let r = m - (l_cb + l_sq + 2 * l + 1);
        [r / (l + 1), l, r % (l + 1)]
    } else {
        let r = m - (l_cb + 2 * l_sq + 3 * l + 1);
        [r / l, r % l, l]
    }
}
